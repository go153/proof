
## Proof user

1. Para construir la imagen.
docker build --tag proof_user:v1 .

2. Para correr el contenedor.
docker run -d --name proof_user -p 8080:8080 proof_user:v1

3. Rutas

* User
/api/v1/user", -> POST
/api/v1/user/{id} -> GET
/api/v1/user/update/{id}  -> PUT
/api/v1/user/delete/{id} -> DELETE

* Message
/api/v1/message  -> POST
/api/v1/message/getall/byuser/{userid} -> GET
/api/v1/message/get/{id}-> GET
/api/v1/message/update/{id} ->  PUT
/api/v1/message/delete/{id} ->  DELETE
