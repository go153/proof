package routes

import (
	"github.com/labstack/echo"
	"gitlab.com/Jota_cepeda/proof_user/internal/handler"
)

func Router(e *echo.Echo) {

	r := e.Group("/api/v1")

	// User
	r.POST("/user", handler.CreateUserHandler)
	r.POST("/user/login", handler.LoginUserHandler)
	r.GET("/user/:id", handler.GetUserHandler)
	r.PUT("/user/update/:id", handler.UpdateUserHandler)
	r.DELETE("/user/delete/:id", handler.DeleteUserHandler)

	// product
	r.POST("/product", handler.CreateMessageHandler)
	r.POST("/product/getall/byuser/:userid", handler.GetAllMessagesByUserHandler)
	r.POST("/product/getall", handler.GetAllHandler)
	r.POST("/product/get/:id", handler.GetMessagesHandler)
	r.PUT("/product/update/:id", handler.UpdateMessageHandler)
	r.DELETE("/product/delete/:id", handler.DeleteMessageHandler)

	// Order
	r.POST("/order", handler.CreateOrderHandler)
	r.POST("/order/:id", handler.GetOrderHandler)
	r.PUT("/order/update/:id", handler.UpdateOrderHandler)
	r.DELETE("/order/delete/:id", handler.DeleteOrderHandler)


}
