package user

import (
	"fmt"
	"os"
	"strings"
)

var messageStorage messageStorageInterface

func init() {
	setMessageStorage()
}

type messageStorageInterface interface {
	InsertMessage(Data *Message) error
	GetMessage(ID string) (*Message, error)
	GetAllMessageByUser(userID string) ([]*Message, error)
	GetAll() ([]*Message, error)
	UpdateMessage(ID string, message *Message) (*Message, error)
	DeleteMessage(ID string) (bool, error)
}

func setMessageStorage() {

	dbEngine := os.Getenv("DB_ENGINE")

	switch strings.ToLower(dbEngine) {
	case "mongodb":
		messageStorage = messageMongoDB{}
	case "postgres":
		fallthrough
	case "oci8":
		fallthrough
	default:
		fmt.Printf("este motor de bd no está configurado aún: %s ", os.Getenv("DB_CONNECTION"))
	}

}
