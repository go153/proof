package user

import (
	"fmt"
	"github.com/labstack/echo"
)

//Message
// CreateMessageProcessor ...
func CreateMessageProcessor(c echo.Context) (*Message, error) {

	message := &Message{}

	err := c.Bind(&message)
	if err != nil {
		fmt.Println("The struct of the object no valid : ", err)
		return message, err
	}

	err = messageStorage.InsertMessage(message)
	if err != nil {
		return message, err
	}

	return message, nil
}

// GetMessageProcessor ...
func GetMessageProcessor(c echo.Context) (*Message, error) {
	fmt.Println("GetMessageProcessor")
	message := &Message{}
	id :=  c.Param("id")

	message, err := messageStorage.GetMessage(id)
	if err != nil {
		return nil, err
	}

	return message, nil
}

// GetAllMessageProcessor ...
func GetAllMessageByUserProcessor(c echo.Context) ([]*Message, error) {

	userID :=  c.Param("userid")

	user, err := messageStorage.GetAllMessageByUser(userID)
	if err != nil {
		return nil, err
	}

	return user, nil
}

// GetAllProcessor ...
func GetAllProcessor(c echo.Context) ([]*Message, error) {

	user, err := messageStorage.GetAll()
	if err != nil {
		return nil, err
	}

	return user, nil
}

// UpdateMessageProcessor ...
func UpdateMessageProcessor(c echo.Context) (*Message, error) {

	id :=  c.Param("id")
	message := &Message{}

	err := c.Bind(&message)
	if err != nil {
		fmt.Println("The struct of the object no valid : ", err)
		return message, err
	}

	message, err = messageStorage.UpdateMessage(id, message)
	if err != nil {
		return message, err
	}

	return message, nil
}

// DeleteMessageProcessor ...
func DeleteMessageProcessor(c echo.Context) (bool, error) {

	id :=  c.Param("id")

	_, err := messageStorage.DeleteMessage(id)
	if err != nil {
		return false, err
	}

	return true, nil
}
