package user

import (
	"fmt"
	"github.com/labstack/echo"
)

// User

// CreateUserProcessor ...
func CreateUserProcessor( c echo.Context) (*User, error) {

	user := &User{}
	err := c.Bind(&user)
	if err != nil {
		fmt.Println("The struct of the object no valid : ", err)
		return user, err
	}

	//TODO CREATE
	err = userStorage.InsertUser(user)
	if err != nil {
		return user, err
	}

	return user, nil
}
// LoginUserProcessor ...
func LoginUserProcessor(c echo.Context) (*User, error) {
	fmt.Println("LoginUserProcessor")
	login := &User{}
	err := c.Bind(&login)
	if err != nil {
		fmt.Println("The struct of the object no valid : ", err)
		return login, err
	}

	user, err := userStorage.GetLogin(login)
	if err != nil {
		return nil, err
	}
	return user, nil
}
// GetUserProcessor ...
func GetUserProcessor(c echo.Context) (*User, error) {

	id := c.Param("id")

	user, err := userStorage.GetUser(id)
	if err != nil {
		return nil, err
	}
	return user, nil
}
// UpdateUserProcessor ...
func UpdateUserProcessor(c echo.Context) (*User, error) {

	user := &User{}
	err := c.Bind(&user)
	if err != nil {
		fmt.Println("The struct of the object no valid : ", err)
		return user, err
	}
	id := c.Param("id")

	user, err = userStorage.UpdateUser(id, user)
	if err != nil {
		return user, err
	}

	return user, nil
}
// DeleteUserProcessor ...
func DeleteUserProcessor(c echo.Context) (bool, error) {

	id := c.Param("id")
	_, err := userStorage.DeleteUser(id)
	if err != nil {
		return false, err
	}

	return true, nil
}

