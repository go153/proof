package user

import (
	"context"
	"encoding/json"
	"fmt"

	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type messageMongoDB struct{}

var (
	messageCollectionName string = "product"
)

func init() {
	var err error
	client, err = getConnection()
	if err != nil {
		log.Fatalln(err)
	}
}

// InsertMessage ...
func (s messageMongoDB) InsertMessage(data *Message) error {

	err := messageWorkMongoDBInsertData(data)
	if err != nil {
		return err
	}

	return nil
}

// GetMessage ...
func (s messageMongoDB) GetMessage(ID string) (*Message, error) {
	fmt.Println("GetMessage")
	userDB, err := messageWorkMongoDBGetOneInstanceData(ID)
	if err != nil {
		return nil, err
	}

	return userDB, nil
}

// GetAllMessageByUser ...
func (s messageMongoDB) GetAllMessageByUser(userID string) ([]*Message, error) {

	messageDB, err := messageWorkMongoDBGetAllInstanceDataByUser(userID)
	if err != nil {
		return nil, err
	}

	return messageDB, nil
}

// GetAll ...
func (s messageMongoDB) GetAll() ([]*Message, error) {

	messageDB, err := messageWorkMongoDBGetAll()
	if err != nil {
		return nil, err
	}

	return messageDB, nil
}


// UpdateMessage ...
func (s messageMongoDB) UpdateMessage(ID string, message *Message) (*Message, error) {

	data, err := json.Marshal(message)
	if err != nil {
		return nil, err
	}

	updateData := make(map[string]interface{})

	err = json.Unmarshal(data, &updateData)
	if err != nil {
		return nil, err
	}

	message, err = messageWorkMongoDBUpdateOneInstanceData(ID, updateData)
	if err != nil {
		return nil, err
	}

	return message, nil
}

// DeleteMessage ...
func (s messageMongoDB) DeleteMessage(ID string) (bool, error) {

	err := messageWorkMongoDBDeleteOneInstanceData(ID)
	if err != nil {
		return false, err
	}

	return true, nil
}

// messageWorkMongoDBInsertData ...
func messageWorkMongoDBInsertData(data *Message) error {

	_, err := userStorage.GetUser(data.UserID)
	if err != nil {
		return err
	}

	collection := client.Database(dataBaseName).Collection(messageCollectionName)

	id, err := collection.InsertOne(context.TODO(), data)
	if err != nil {
		return err
	}

	data.ID = id.InsertedID.(primitive.ObjectID).Hex()

	return nil
}

// messageWorkMongoDBGetOneInstanceData ...
func messageWorkMongoDBGetOneInstanceData(ID string) (*Message, error) {
	fmt.Println("messageWorkMongoDBGetOneInstanceData")
	message := &Message{}

	docID, err := primitive.ObjectIDFromHex(ID)
	if err != nil {
		return message, err
	}

	filter := bson.D{{"_id", docID}}
	fmt.Println("filter")
	fmt.Println(filter)
	collection := client.Database(dataBaseName).Collection(messageCollectionName)

	if err := collection.FindOne(context.TODO(), filter).Decode(message); err != nil {
		return nil, err
	}

	message.ID = ID

	return message, nil
}

// messageWorkMongoDBGetAllInstanceDataByUser ...
func messageWorkMongoDBGetAllInstanceDataByUser(ID string) ([]*Message, error) {

	results := []*Message{}

	_, err := userStorage.GetUser(ID)
	if err != nil {
		return results, err
	}

	findOptions := options.Find()
	findOptions.SetLimit(10)

	filter := bson.D{{"userID", ID}}

	collection := client.Database(dataBaseName).Collection(messageCollectionName)

	data, err := collection.Find(context.TODO(), filter, findOptions)
	if err != nil {
		return nil, err
	}

	for data.Next(context.TODO()) {

		msg := &Message{}
		err := data.Decode(msg)
		if err != nil {
			return nil, err
		}

		results = append(results, msg)

	}

	return results, nil
}

// messageWorkMongoDBGetAll ...
func messageWorkMongoDBGetAll() ([]*Message, error) {

	results := []*Message{}

	findOptions := options.Find()
	findOptions.SetLimit(10)

	filter := bson.D{{ }}

	collection := client.Database(dataBaseName).Collection(messageCollectionName)

	data, err := collection.Find(context.TODO(), filter, findOptions)
	if err != nil {
		return nil, err
	}

	for data.Next(context.TODO()) {

		msg := &Message{}
		err := data.Decode(msg)
		if err != nil {
			return nil, err
		}

		results = append(results, msg)

	}

	return results, nil
}



// workMongoDBUpdateOneInstanceData ...
func messageWorkMongoDBUpdateOneInstanceData(ID string, data map[string]interface{}) (*Message, error) {

	message := &Message{}

	collection := client.Database(dataBaseName).Collection(messageCollectionName)

	docID, err := primitive.ObjectIDFromHex(ID)
	if err != nil {
		return message, err
	}

	filter := bson.D{{"_id", docID}}

	update := bson.M{}
	update = data

	result := collection.FindOneAndUpdate(context.TODO(), filter, bson.M{"$set": update}, options.FindOneAndUpdate().SetReturnDocument(1))
	if result.Err() != nil {
		return message, err
	}

	err = result.Decode(message)
	if err != nil {
		return message, err
	}

	message.ID = ID

	return message, nil

}

// workMongoDBDeleteOneInstanceData ...
func messageWorkMongoDBDeleteOneInstanceData(ID string) error {

	docID, err := primitive.ObjectIDFromHex(ID)
	if err != nil {
		return err
	}

	collection := client.Database(dataBaseName).Collection(messageCollectionName)

	filter := bson.D{{"_id", docID}}

	_, err = collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		return err
	}

	return nil
}
