package user

// User model
type Message struct {
	ID     string  `json:"id" bson:"_id,omitempty"`
	UserID string `json:"userID,omitempty" bson:"userID"`
	Name   string `json:"name,omitempty" bson:"name"`
	SKU    string `json:"sku" bson:"sku"`
	Amount string `json:"amount,omitempty" bson:"amount"`
}
