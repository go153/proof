package handler

import (
	"github.com/labstack/echo"
	processor "gitlab.com/Jota_cepeda/proof_user/internal/order"
	"net/http"
)

// CreateOrderHandler - POST - /api/v1/order
func CreateOrderHandler(c echo.Context)  error {

	resp, err := processor.CreateOrderProcessor(c)
	if err != nil {

		return c.JSON(http.StatusAccepted, resp)
	}
	return c.JSON(http.StatusOK, resp)

}

// GetOrderHandler - GET - /api/v1/order/{id}
func GetOrderHandler(c echo.Context) error {

	resp, err := processor.GetOrderProcessor(c)
	if err != nil {

		return c.JSON(http.StatusAccepted, resp)
	}
	return c.JSON(http.StatusOK, resp)

}

// UpdateOrderHandler - PUT - /api/v1/order/update/{id}
func UpdateOrderHandler(c echo.Context) error {

	resp, err := processor.UpdateOrderProcessor(c)
	if err != nil {

		return c.JSON(http.StatusAccepted, resp)
	}
	return c.JSON(http.StatusOK, resp)

}

// DeleteOrderHandler - DELETE - /api/v1/order/delete/{id}
func DeleteOrderHandler(c echo.Context) error {

	resp, err := processor.DeleteOrderProcessor(c)
	if err != nil {

		return c.JSON(http.StatusAccepted, resp)
	}
	return c.JSON(http.StatusOK, resp)

}
