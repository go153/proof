package handler

import (
	"fmt"
	"github.com/labstack/echo"
	processor "gitlab.com/Jota_cepeda/proof_user/internal/user"
	"net/http"
)

// CreateUserHandler - POST - /api/v1/user
func CreateUserHandler(c echo.Context) error {

	resp, err := processor.CreateUserProcessor(c)
	if err != nil {

		return c.JSON(http.StatusAccepted, resp)
	}
	return c.JSON(http.StatusOK, resp)
}
// LoginUserHandler - GET - /api/v1/user/{id}
func LoginUserHandler(c echo.Context) error {
	fmt.Println("LoginUserHandler")
	resp, err := processor.LoginUserProcessor(c)
	if err != nil {
		return c.JSON(http.StatusAccepted, resp)
	}
	return c.JSON(http.StatusOK, resp)

}


// GetUserHandler - GET - /api/v1/user/{id}
func GetUserHandler(c echo.Context) error {

	resp, err := processor.GetUserProcessor(c)
	if err != nil {
		return c.JSON(http.StatusAccepted, resp)
	}
	return c.JSON(http.StatusOK, resp)

}

// UpdateUserHandler - PUT - /api/v1/user/update/{id}
func UpdateUserHandler(c echo.Context) error {


	resp, err := processor.UpdateUserProcessor(c)
	if err != nil {
		return c.JSON(http.StatusAccepted, resp)
	}
	return c.JSON(http.StatusOK, resp)

}

// DeleteUserHandler - DELETE - /api/v1/user/delete/{id}
func DeleteUserHandler(c echo.Context) error {


	resp, err := processor.DeleteUserProcessor(c)
	if err != nil {
		return c.JSON(http.StatusAccepted, resp)
	}
	return c.JSON(http.StatusOK, resp)


}

// MESSAGE
// CreateMessageHandler - POST - /api/v1/message/
func CreateMessageHandler(c echo.Context) error {


	resp, err := processor.CreateMessageProcessor(c)
	if err != nil {
		return c.JSON(http.StatusAccepted, resp)
	}
	return c.JSON(http.StatusOK, resp)

}

// GetMessagesHandler - GET - /api/v1/message/{id}
func GetMessagesHandler(c echo.Context)error  {
	fmt.Println("GetMessagesHandler")
	resp, err := processor.GetMessageProcessor(c)
	if err != nil {
		return c.JSON(http.StatusAccepted, resp)
	}
	return c.JSON(http.StatusOK, resp)

}

// GetAllMessagesByUserHandler - POST - /api/v1/message/{id}
func GetAllMessagesByUserHandler(c echo.Context)error {

	resp, err := processor.GetAllMessageByUserProcessor(c)
	if err != nil {
		return c.JSON(http.StatusAccepted, resp)
	}
	return c.JSON(http.StatusOK, resp)

}
// GetAllHandler - POST - /api/v1/product/getall
func GetAllHandler(c echo.Context)error {

	resp, err := processor.GetAllProcessor(c)
	if err != nil {
		return c.JSON(http.StatusAccepted, resp)
	}
	return c.JSON(http.StatusOK, resp)

}
// UpdateMessageHandler - PUT - /api/v1/message/update/{id}
func UpdateMessageHandler(c echo.Context)error {


	resp, err := processor.UpdateMessageProcessor(c)
	if err != nil {
		return c.JSON(http.StatusAccepted, resp)
	}
	return c.JSON(http.StatusOK, resp)

}

// DeleteMessageHandler - DELETE - /api/v1/message/delete/{id}
func DeleteMessageHandler(c echo.Context)error {

	//vars := mux.Vars(r)
	//k := vars["id"]

	resp, err := processor.DeleteMessageProcessor(c)
	if err != nil {
		return c.JSON(http.StatusAccepted, resp)
	}
	return c.JSON(http.StatusOK, resp)

}
