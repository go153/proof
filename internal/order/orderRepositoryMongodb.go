package order

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type orderMongoDB struct{}

var (
	dataBaseName   string = "Inventory"
	collectionName string = "order"
	client         *mongo.Client
)

func init() {
	var err error
	client, err = getConnection()
	if err != nil {
		log.Fatalln(err)
	}
}

// InsertOrder ...
func (s orderMongoDB) InsertOrder(data *Order) error {

	err := orderWorkMongoDBInsertData(data)
	if err != nil {
		return err
	}

	return nil
}

// GetOrder ...
func (s orderMongoDB) GetOrder(ID string) ([]*Order, error) {

	userDB, err := orderWorkMongoDBGetOneInstanceData(ID)
	if err != nil {
		return nil, err
	}

	return userDB, nil
}

// UpdateOrder ...
func (s orderMongoDB) UpdateOrder(ID string, user *Order) (*Order, error) {

	data, err := json.Marshal(user)
	if err != nil {
		return nil, err
	}

	updateData := make(map[string]interface{})

	err = json.Unmarshal(data, &updateData)
	if err != nil {
		return nil, err
	}

	user, err = orderWorkMongoDBUpdateOneInstanceData(ID, updateData)
	if err != nil {
		return nil, err
	}

	return user, nil
}

// DeleteOrder ...
func (s orderMongoDB) DeleteOrder(ID string) (bool, error) {

	//err := orderWorkMongoDBDeleteOneInstanceData(ID)
	//if err != nil {
	//	return false, err
	//}

	return true, nil
}

// getConnection ...
func getConnection() (*mongo.Client, error) {

	stringConnection := os.Getenv("DB_STRING_CONNECTION")

	clientOpts := options.Client().ApplyURI(fmt.Sprintf(stringConnection))

	client, err := mongo.Connect(context.TODO(), clientOpts)
	if err != nil {
		return nil, err
	}

	err = client.Ping(context.TODO(), nil)
	if err != nil {
		return nil, err
	}

	return client, nil
}

// orderWorkMongoDBInsertData ...
func orderWorkMongoDBInsertData(data *Order) error {

	collection := client.Database(dataBaseName).Collection(collectionName)

	id, err := collection.InsertOne(context.TODO(), data)
	if err != nil {
		return err
	}

	data.ID = id.InsertedID.(primitive.ObjectID).Hex()

	return nil
}

// orderWorkMongoDBGetOneInstanceData ...
func orderWorkMongoDBGetOneInstanceData(ID string) ([]*Order, error) {
	results := []*Order{}

	findOptions := options.Find()
	findOptions.SetLimit(10)

	filter := bson.D{{"user_id", ID}}

	collection := client.Database(dataBaseName).Collection(collectionName)

	data, err := collection.Find(context.TODO(), filter, findOptions)
	if err != nil {
		return nil, err
	}

	for data.Next(context.TODO()) {

		msg := &Order{}
		err := data.Decode(msg)
		if err != nil {
			return nil, err
		}

		results = append(results, msg)

	}

	return results, nil
}

// orderWorkMongoDBUpdateOneInstanceData ...
func orderWorkMongoDBUpdateOneInstanceData(ID string, data map[string]interface{}) (*Order, error) {

	user := &Order{}

	collection := client.Database(dataBaseName).Collection(collectionName)

	docID, err := primitive.ObjectIDFromHex(ID)
	if err != nil {
		return user, err
	}

	filter := bson.D{{"_id", docID}}

	update := bson.M{}
	update = data

	result := collection.FindOneAndUpdate(context.TODO(), filter, bson.M{"$set": update}, options.FindOneAndUpdate().SetReturnDocument(1))
	if result.Err() != nil {
		return user, err
	}

	err = result.Decode(user)
	if err != nil {
		return user, err
	}

	user.ID = ID

	return user, nil
}

// orderWorkMongoDBDeleteOneInstanceData ...
//func orderWorkMongoDBDeleteOneInstanceData(ID string) error {
//
//	collection := client.Database(dataBaseName).Collection(collectionName)
//
//	dataMesagge, err :=  messageStorage.GetAllMessageByUser(ID)
//	if err != nil {
//		return err
//	}
//
//	for _, value := range dataMesagge {
//
//		_, err := messageStorage.DeleteMessage(value.ID)
//		if err != nil {
//			return err
//		}
//
//	}
//
//	docID, err := primitive.ObjectIDFromHex(ID)
//	if err != nil {
//		return err
//	}
//
//	filter := bson.D{{"_id", docID}}
//
//	_, err = collection.DeleteOne(context.TODO(), filter)
//	if err != nil {
//		log.Fatal(err)
//	}
//
//	return nil
//}
