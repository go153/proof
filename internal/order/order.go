package order

// User model
type Order struct {
	ID       string  `json:"id" bson:"_id,omitempty"`
	UserID    string `json:"user_id" bson:"user_id"`
	ProductID string `json:"product_id" bson:"product_id"`
	Cantidad string `json:"cantidad" bson:"cantidad"`
}
