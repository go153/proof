package order

import (
	"fmt"
	"github.com/labstack/echo"
)

// order

// CreateOrderProcessor ...
func CreateOrderProcessor(c echo.Context) (*Order, error) {

	order := &Order{}

	err := c.Bind(&order)
	if err != nil {
		fmt.Println("The struct of the object no valid : ", err)
		return order, err
	}

	//TODO CREATE
	err = orderStorage.InsertOrder(order)
	if err != nil {
		return order, err
	}

	return order, nil
}

// GetOrderProcessor ...
func GetOrderProcessor(c echo.Context) ([]*Order, error) {

	id :=  c.Param("id")

	user, err := orderStorage.GetOrder(id)
	if err != nil {
		return nil, err
	}
	return user, nil
}

// UpdateOrderProcessor ...
func UpdateOrderProcessor(c echo.Context) (*Order, error) {

	order := &Order{}
	id :=  c.Param("id")

	err := c.Bind(&order)
	if err != nil {
		fmt.Println("The struct of the object no valid : ", err)
		return order, err
	}

	order, err = orderStorage.UpdateOrder(id, order)
	if err != nil {
		return order, err
	}

	return order, nil
}

// DeleteOrderProcessor ...
func DeleteOrderProcessor(c echo.Context) (bool, error) {

	id :=  c.Param("id")

	_, err := orderStorage.DeleteOrder(id)
	if err != nil {
		return false, err
	}

	return true, nil
}


