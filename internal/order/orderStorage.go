package order

import (
	"fmt"
	"os"
	"strings"
)

var orderStorage orderStorageInterface

func init() {
	setUserStorage()
}

type orderStorageInterface interface {
	InsertOrder(user *Order) error
	GetOrder(ID string) ([]*Order, error)
	UpdateOrder(ID string, user *Order) (*Order, error)
	DeleteOrder(ID string) (bool, error)
}

// setUserStorage ...
func setUserStorage() {

	dbEngine := os.Getenv("DB_ENGINE")

	switch strings.ToLower(dbEngine) {
	case "mongodb":
		orderStorage = orderMongoDB{}
	case "postgres":
		fallthrough
	case "oci8":
		fallthrough
	default:
		fmt.Printf("este motor de bd no está configurado aún: %s", os.Getenv("DB_CONNECTION"))
	}

}
