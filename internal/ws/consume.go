package ws

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net/http"
)

func ConsumeWS(json_bytes []byte, url string, token string) ([]byte, error) {

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(json_bytes))
	if token != "" {
		req.Header.Set("Authorization", token)
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("no se  puedo enviar la petición: %v  -- log: ", err)
		return nil, err
	}

	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)

}
func ConsumeWSBasicAuth(json_bytes []byte, url string, user string, pass string) ([]byte, error) {

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(json_bytes))

	req.SetBasicAuth(user, pass)

	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("no se  puedo enviar la petición: %v  -- log: ", err)
		return nil, err
	}

	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)

}
func ConsumeWSSoap(xml_bytes []byte, url string) ([]byte, error) {

	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(xml_bytes))
	req.Header.Add("Content-Type", "text/xml")

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	client := &http.Client{
		Transport: tr,
	}

	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("no se  puedo enviar la petición: %v  -- log: ", err)
		return nil, err
	}

	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)

}
